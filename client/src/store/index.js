import Vue from 'vue';
import Vuex from 'vuex';
import { fetchData } from '@/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: [],
  },
  getters: {

  },
  actions: {
    getPosts({ commit }, platform, gamertag) {
      return new Promise((resolve, reject) => {
        fetchData()
          .then((res) => {
            const endpoint = res;
            console.log(endpoint);
            commit('SET_POSTS', endpoint);
            resolve(res);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },
  },
  mutations: {
    SET_POSTS(state, data) {
      Vue.set(state, 'data', data);
    },
  },
});

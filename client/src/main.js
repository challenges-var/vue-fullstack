// Plugins
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueToasted from 'vue-toasted';
// Vue
import Vue from 'vue'
import router from './router';
import store from './store';
import App from './App.vue'
import './styles/main.scss';

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(VueToasted, {
  iconPack: 'fontawesome',
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

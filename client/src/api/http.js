import axios from 'axios';

/* eslint-disable */
export const HTTP = axios.create({
  headers: {
    'Content-Type': `application/json`,
  },
});
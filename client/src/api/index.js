import { HTTP } from './http';

/* eslint-disable */
export const fetchData = (platform, gamertag) => new Promise((resolve, reject) => HTTP.get(`/api/v1/profile/${platform}/${gamertag}`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));
Vue Fullstack: express dotenv morgan node-fetch concurently, Vue Vuex.
Api call to find gamers profile and stats.

Visit: https://dreamteam.gg/apex/leaderboards to fin gamers username

# Setup
clone the project
On the full project root run: npm i --> server side

On the client folder root run: npm i -- client side

# Dev Mode
On the root project run: npm run dev

The backend and the frontend will run on the same time
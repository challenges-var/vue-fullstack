const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');

// Load the config file
dotenv.config({
  path: './config.env',
});

const app = express();

// Dev Logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Profile routes
app.use('/api/v1/profile', require('./routes/profile'));

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log(`Server Running In ${process.env.NODE_ENV} Mode On Port ${port}!`);
});